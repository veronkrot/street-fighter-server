const genericAction = (req, resp, next, callback) => {
    try {
        callback();
    } catch (e) {
        resp.error = e;
    } finally {
        next();
    }
}

const genericRoutes = (router, service, createValid, updateValid, responseMiddleware) => {
    router.post('', createValid, (req, resp, next) => {
        genericAction(req, resp, next, () => {
            resp.locals = service.create(req.body);
        });
    }, responseMiddleware);

    router.get('', (req, resp, next) => {
        genericAction(req, resp, next, () => {
            resp.locals = service.getAll();
        });
    }, responseMiddleware);

    router.get('/:id', (req, resp, next) => {
        genericAction(req, resp, next, () => {
            resp.locals = service.search({id: req.params.id});
        });
    }, responseMiddleware);

    router.put('/:id', updateValid, (req, resp, next) => {
        genericAction(req, resp, next, () => {
            resp.locals = service.update(req.params.id, req.body);
        });
    }, responseMiddleware);

    router.delete('/:id', (req, resp, next) => {
        genericAction(req, resp, next, () => {
            resp.locals = service.delete(req.params.id);
        });
    }, responseMiddleware);
}

module.exports = genericRoutes;
