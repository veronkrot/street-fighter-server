const express = require('express');
const UserService = require('../services/userService');
const genericRoutes = require("./genericRoutes");
const {createUserValid, updateUserValid} = require('../middlewares/user.validation.middleware');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = express.Router();

genericRoutes(router, UserService, createUserValid, updateUserValid, responseMiddleware);

module.exports = router;
