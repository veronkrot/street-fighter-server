const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        res.locals = AuthService.login(req.body);
    } catch (err) {
        res.error = err;
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;
