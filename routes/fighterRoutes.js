const { Router } = require('express');
const FighterService = require('../services/fighterService');
const genericRoutes = require("./genericRoutes");
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

genericRoutes(router, FighterService, createFighterValid, updateFighterValid, responseMiddleware);

module.exports = router;
