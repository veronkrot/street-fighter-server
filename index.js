const express = require('express')
const cors = require('cors');
require('./config/db');

const app = express()

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const routes = require('./routes/index');
const {responseMiddleware} = require("./middlewares/response.middleware");
routes(app);

app.use('/', express.static('./client/build'));

app.use((error, req, resp, next) => {
    responseMiddleware(req, resp, next, error);
});

const port = 3050;
app.listen(port, () => {});

exports.app = app;
