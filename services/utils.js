const utils = {
    error400: (message) => {
        let error = new Error(message);
        error.statusCode = 400;
        return error;
    },

    error404: (message) => {
        let error = new Error(message);
        error.statusCode = 404;
        return error;
    },
    isUniqueField: (uniqueFields, objects, object) => {
        return uniqueFields.every((field) => {
            let fieldsInDb = objects.map(x => String(x[field]).toLowerCase());
            const fieldValue = object[field];
            if (fieldValue) {
                if (fieldsInDb.includes(String(fieldValue).toLowerCase())) {
                    return false;
                }
            }
            return true;
        })
    }
}

module.exports = {
    utils
}
