const {utils} = require("./utils");
const {UserRepository} = require('../repositories/userRepository');

const uniqueFields = ['email', 'phoneNumber'];

class UserService {
    search(search) {
        const item = UserRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(data) {
        if (!utils.isUniqueField(uniqueFields, this.getAll(), data)) {
            throw utils.error400(`Fields ${uniqueFields} should be unique across all users!`);
        }
        return UserRepository.create(data);
    }

    getAll() {
        return UserRepository.getAll();
    }

    update(id, dataToUpdate) {
        let storedUser = this.search({id});
        if (!storedUser) {
            throw utils.error404(`User with id ${id} not found`);
        }
        if (!utils.isUniqueField(this.getAll(), dataToUpdate)) {
            throw utils.error400(`Fields ${uniqueFields} should be unique across all users!`);
        }
        return UserRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return UserRepository.delete(id);
    }
}

module.exports = new UserService();
