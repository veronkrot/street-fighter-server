const {utils} = require("./utils");
const {FighterRepository} = require('../repositories/fighterRepository');

const defaultFighter = {
    health: 100
};

const uniqueFields = ['name'];

class FighterService {
    search(search) {
        const item = FighterRepository.getOne(search);
        if (!item) {
            return null;
        }
        return item;
    }

    create(data) {
        let fighterToCreate = {...defaultFighter, ...data};
        if (!utils.isUniqueField(uniqueFields, this.getAll(), fighterToCreate)) {
            throw utils.error400(`Fields ${uniqueFields} should be unique across all fighters!`);
        }
        return FighterRepository.create(fighterToCreate);
    }

    getAll() {
        return FighterRepository.getAll();
    }

    update(id, dataToUpdate) {
        if (!utils.isUniqueField(this.getAll(), dataToUpdate)) {
            throw utils.error400(`Fields ${uniqueFields} should be unique across all fighters!`);
        }
        return FighterRepository.update(id, dataToUpdate);
    }

    delete(id) {
        return FighterRepository.delete(id);
    }
}

module.exports = new FighterService();
