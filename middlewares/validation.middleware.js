const {utils} = require("../services/utils");

const getParams = (obj) => {
    return Object.keys(obj).filter(el => el !== 'id');
}

const getRequiredParams = (arr) => {
    return arr.filter(p => p.required).map(p => p.field);
}

const getOptionalParams = (arr) => {
    return arr.filter(p => p.optional).map(p => p.field);
}

const hasAllRequiredParams = (params, requiredParams, optionalParams) => {
    const invalidParams = [];
    params.forEach((param) => {
        if (!requiredParams.includes(param)) {
            if (!optionalParams.includes(param)) {
                invalidParams.push(param);
            }
        }
    })
    if (invalidParams.length !== 0) {
        console.log('Invalid object params', invalidParams);
    }
    return invalidParams.length === 0;

};

const sendErrorResponse = (res, errorText) => {
    let error = utils.error400(errorText);
    res.error = error;
    throw error;
}

const isValidField = (res, validationParam, fieldValue) => {
    const isFieldNotSpecified = fieldValue === undefined || fieldValue === null || fieldValue.length < 1;
    if (isFieldNotSpecified) {
        if (validationParam.shouldBePresent) {
            sendErrorResponse(res, `Field ${validationParam.field} should be present!`);
            return false;
        }
        if (validationParam.required) {
            sendErrorResponse(res, `Field ${validationParam.field} is required!`);
            return false;
        } else {
            return true;
        }
    } else {
        if (!validationParam.shouldBePresent && !validationParam.optional) {
            sendErrorResponse(res, `Field ${validationParam.field} shouldn't be specified!`);
            return false;
        }
    }
    if (!isFieldNotSpecified && fieldValue && fieldValue.length < validationParam.minLength) {
        sendErrorResponse(res, `Min length for ${validationParam.field} should be ${validationParam.minLength}`);
        return false;
    }
    if (validationParam.format) {
        let isMatchingRegex = validationParam.format.test(String(fieldValue).toLowerCase());
        if (!isMatchingRegex) {
            sendErrorResponse(res, `Field ${validationParam.field} doesn't match format ${validationParam.format}`);
            return false;
        }
    }
    if (validationParam.min && validationParam.max) {
        // todo: split this logic, to allow specifying just min value or just max value
        if (isNaN(fieldValue)) {
            sendErrorResponse(res, `Field ${validationParam.field} is not a number`);
            return false;
        }
        if (fieldValue < validationParam.min || fieldValue > validationParam.max) {
            sendErrorResponse(res, `Field ${validationParam.field} is out of bounds [${validationParam.min}, ${validationParam.max}]`);
            return false;
        }
    }
    return true;
}

const createValid = (isAllRequiredParams, requiredParams, optionalParams, validationParams, res, data) => {
    if (!isAllRequiredParams) {
        sendErrorResponse(res, `Request params should include only: ${requiredParams} and optionally ${optionalParams}`);
        return;
    }
    for (const param of validationParams) {
        const fieldValue = data[param.field];
        if (!isValidField(res, param, fieldValue)) {
            return;
        }
    }
}

const updateValid = (dataFields, param, res, requiredParams, optionalParams, data, validationParams) => {
    const isUserParamsContainReqParams = dataFields.every(i => param.includes(i));
    if (!isUserParamsContainReqParams) {
        sendErrorResponse(res, `Request params should include only: ${requiredParams} and optionally ${optionalParams}`);
        return;
    }
    for (const objField in data) {
        for (const param of validationParams) {
            if (objField === param.field) {
                const fieldValue = data[param.field];
                if (!isValidField(res, param, fieldValue)) {
                    return;
                }
            }
        }
    }
}

module.exports = {
    getParams,
    isValidField,
    sendErrorResponse,
    getRequiredParams,
    getOptionalParams,
    hasAllRequiredParams,
    createValid,
    updateValid
}
