const {getOptionalParams} = require("./validation.middleware");
const {
    getParams,
    getRequiredParams,
    createValid,
    hasAllRequiredParams,
    updateValid
} = require("./validation.middleware");
const {user} = require('../models/user');

const userValidationParams = [
    {
        field: 'id',
        shouldBePresent: false,
        required: false
    },
    {
        field: 'firstName',
        shouldBePresent: true,
        minLength: 1,
        required: true
    },
    {
        field: 'lastName',
        shouldBePresent: true,
        minLength: 1,
        required: true
    },
    {
        field: 'email',
        shouldBePresent: true,
        minLength: 1,
        format: /^\w+@gmail.com$/,
        required: true
    },
    {
        field: 'phoneNumber',
        shouldBePresent: true,
        minLength: 1,
        format: /^\+380\d{9}$/,
        required: true
    },
    {
        field: 'password',
        shouldBePresent: true,
        minLength: 3,
        required: true
    }
];
const userParams = getParams(user);
const requiredUserParams = getRequiredParams(userValidationParams);
const optionalUserParams = getOptionalParams(userValidationParams);


const createUserValid = (req, res, next) => {
    try {
        const data = req.body;
        const dataFields = Object.keys(data);
        createValid(hasAllRequiredParams(dataFields, userParams, optionalUserParams), requiredUserParams, optionalUserParams, userValidationParams, res, data);
        next();
    } catch (e) {
        next(e);
    }
}

const updateUserValid = (req, res, next) => {
    try {
        const data = req.body;
        const dataFields = Object.keys(data);
        updateValid(dataFields, userParams, res, requiredUserParams, optionalUserParams, data, userValidationParams);
        next();
    } catch (e) {
        next(e);
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
