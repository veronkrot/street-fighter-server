const {fighter} = require('../models/fighter');
const {
    getParams,
    getRequiredParams,
    getOptionalParams,
    createValid,
    hasAllRequiredParams,
    updateValid
} = require("./validation.middleware");

const fighterValidationParams = [
    {
        field: 'id',
        shouldBePresent: false,
        required: false
    },
    {
        field: 'name',
        shouldBePresent: true,
        required: true
    },
    {
        field: 'health',
        shouldBePresent: false,
        required: false,
        optional: true,
        min: 80,
        max: 120
    },
    {
        field: 'power',
        shouldBePresent: true,
        required: true,
        min: 1,
        max: 100
    },
    {
        field: 'defense',
        shouldBePresent: true,
        required: true,
        min: 1,
        max: 10
    }
];

const fighterParams = getParams(fighter);
const requiredFighterParams = getRequiredParams(fighterValidationParams);
const optionalFighterParams = getOptionalParams(fighterValidationParams);

const createFighterValid = (req, res, next) => {
    try {
        const data = req.body;
        const dataFields = Object.keys(data);
        const isAllRequiredParams = hasAllRequiredParams(dataFields, requiredFighterParams, optionalFighterParams);
        createValid(isAllRequiredParams, requiredFighterParams, optionalFighterParams, fighterValidationParams, res, data);
        next();
    } catch (e) {
        next(e);
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        const data = req.body;
        const dataFields = Object.keys(data);
        updateValid(dataFields, fighterParams, res, requiredFighterParams, optionalFighterParams, data, fighterValidationParams);
        next();
    } catch (e) {
        next(e);
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
