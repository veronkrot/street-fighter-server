const responseMiddleware = (req, res, next) => {
    let statusCode = 200;
    let data = res.locals;
    if (res.error) {
        statusCode = res.error.statusCode;
        data = {
            error: true,
            message: res.error.message
        }
    }
    res.status(statusCode).send(data);
    next();
}

exports.responseMiddleware = responseMiddleware;
